library(bnlearn)
library(xlsx)
#This function gives a random graph
create_graph <- function(){
  graph_control_var = 0
  let <- c("praf", "pmek", "plcg", "PIP2", "PIP3", "p44.42", "pakts473", "PKA", "PKC", "P38", "pjnk")
  new_graph <- matrix(0, ncol = 11, nrow = 11, dimnames = list(let[1:11], let[1:11]))
  number_of_edges = floor(runif(1, min=1, max=110))
  x <- sort(floor(runif(number_of_edges, min=1, max=11)))
  y <- sort(floor(runif(number_of_edges, min=1, max=11)))
  temp <- 0
  for(i in 1:number_of_edges){
    if(x[i] != y[i]){
      new_graph[x[i], y[i]] <- 1
    }
    #if(new_graph[y[i], x[i]] == 1)
      #new_graph[y[i], x[i]] <- 0
  }
  
  #check loops
  
  e <- empty.graph(let[1:11])
  amat(e) <- new_graph
  return(e)
}

scoring <- function(e, df){
  
}

df <- read.xlsx("1. cd3cd28.xls", sheetName = "Sheet1")


e <- create_graph()